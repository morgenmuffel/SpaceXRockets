//
//  LaunchesModel.swift
//  SpaceXRockets
//
//  Created by Maksim on 12.04.2022.
//

import Foundation

struct LaunchesModel: Codable {
    let fairings: Fairings?
    let links: Links?
    let static_fire_date_utc: String?
    let static_fire_date_unix: Int?
    let net: Bool?
    let window: Int?
    let rocket: String?
    let success: Bool?
    let failures: [Failures]?
    let details: String?
    let crew: Crew?
    let ships: Ships?
    let capsules: Capsules?
    let payloads: Payloads?
    let launchpad: String?
    let flight_number: Int?
    let name: String?
    let date_utc: String?
    let date_unix: Int?
    let date_local: String?
    let date_precision: String?
    let upcoming: Bool?
    let cores: Cores?
    let auto_update: Bool?
    let tbd: Bool?
    let launch_library_id: Int?
    let id: String?
    
    struct Fairings: Codable {
        let reused: Bool?
        let recovery_attempt: Bool?
        let recovered: Bool?
        let ships: Ships?
    }

    struct Ships: Codable {
        
    }

    struct Links: Codable {
        let patch: Patch?
        let reddit: Reddit?
        let flickr: Flickr?
        let presskit: String?
        let webcast: String?
        let youtube_id: String?
        let article: String?
        let wikipedia: String?
        
        struct Patch: Codable {
            let small: String?
            let large: String?
        }

        struct Reddit: Codable {
            let campaign: String?
            let launch: String?
            let media: String?
            let recovery: String?
        }

        struct Flickr: Codable {
            let small: Small?
            let original: Original?
            
            struct Small: Codable {
               
            }

            struct Original: Codable {
                // данные не везде, а где есть - представлены таблицей в виде "Index 0 ... Index N"
            }
        }
    }

    struct Failures: Codable {
        let time: Int?
        let altitude: Int?
        let reason: String?
    }
   
    struct Crew: Codable {
        // данные не везде, а где есть - представлены таблицей в виде "Index 0 ... Index N"
    }

    struct Capsules: Codable {
      
    }
    
    struct Payloads: Codable {
        
    }

    struct Cores: Codable {
        // данные есть не везде, а если и есть, то с вложенным "Index 0"
    }

}

