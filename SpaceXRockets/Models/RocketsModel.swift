//
//  RocketsModel.swift
//  SpaceXRockets
//
//  Created by Maksim on 12.04.2022.
//

import Foundation

struct RocketsModel: Codable {
    let height: Height?
    let diameter: Diameter?
    let mass: Mass?
    let first_stage: FirstStage?
    let second_stage: SecondStage?
    let engines: Engines?
    let landing_legs: LandingLegs?
    let payload_weights: [PayloadWeights]?
//    let flickrImages: FlickrImages?
    let name: String?
    let type: String?
    let active: Bool?
    let stages: Int?
    let boosters: Int?
    let cost_per_launch: Int?
    let success_rate_pct: Float?
    let first_flight: String?
    let country: String?
    let company: String?
    let wikipedia: String?
    let description: String?
    let id: String?
    
    struct Height: Codable {
        let meters: Float?
        let feet: Float?
    }

    struct Diameter: Codable {
        let meters: Float?
        let feet: Float?
    }

    struct Mass: Codable {
        let kg: Int?
        let lb: Int?
    }

    struct FirstStage: Codable {
        let thrust_sea_level: ThrustSeaLevel?
        let thrust_vacuum: ThrustVacuum?
        let reusable: Bool?
        let engines: Int?
        let fuel_amount_tons: Float?
        let burn_time_sec: Int?
        
        struct ThrustSeaLevel: Codable {
            let kN: Int?
            let lbf: Int?
        }

        struct ThrustVacuum: Codable {
            let kN: Int?
            let lbf: Int?
        }
    }

    struct SecondStage: Codable {
        let thrust: Thrust?
        let payloads: Payloads?
        let reusable: Bool?
        let engines: Int?
        let fuel_amount_tons: Float?
        let burn_time_sec: Int?
        
        struct Thrust: Codable {
            let kN: Int?
            let lbf: Int?
        }

        struct Payloads: Codable {
            let composite_fairing: CompositeFairing?
            let option1: String?
        }
        
        struct CompositeFairing: Codable {
            let height: Height?
            let diameter: Diameter?
        }
    }

    struct Engines: Codable {
        let isp: Isp?
        let thrust_sea_level: ThrustSeaLevel?
        let thrust_vacuum: ThrustVacuum?
        let number: Int?
        let type: String?
        let version: String?
        let layout: String?
        let engine_loss_max: Int?
        let propellant_1: String?
        let propellant_2: String?
        let thrust_to_weight: Float?
        
        struct Isp: Codable {
            let sea_level: Int?
            let vacuum: Int?
        }
        
        struct ThrustSeaLevel: Codable {
            let kN: Int?
            let lbf: Int?
        }

        struct ThrustVacuum: Codable {
            let kN: Int?
            let lbf: Int?
        }
    }

    struct LandingLegs: Codable {
        let number: Int?
        let material: String?
    }

    struct PayloadWeights: Codable {
        let id: String?
        let name: String?
        let kg: Int?
        let lb: Int?
    }

    // - MARK: Понадобится?

//    struct FlickrImages: Codable {
//
//        // здесь разное количество индексов, подумать.
//
//    }
}
