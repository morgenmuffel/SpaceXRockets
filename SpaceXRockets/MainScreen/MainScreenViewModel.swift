//
//  MainScreenViewModel.swift
//  SpaceXRockets
//
//  Created by Maksim on 13.04.2022.
//

import Foundation
import Alamofire

class MainScreenViewModel: MainScreenViewModelProtocol {
    
    init() {
        requestManager()
    }
    
}

private extension MainScreenViewModel {
   
    func requestManager() {
        AF.request(
            "https://api.spacexdata.com/v4/rockets"
        ).responseDecodable(of: [RocketsModel].self) { response in

            switch response.result {
            case .success(let rockets):
                print(rockets.count)
                print(rockets[0].first_stage?.fuel_amount_tons)
                print(rockets[0].height?.meters)
            case .failure(let error):
                print(error)
            }
        }

    }
    
}
