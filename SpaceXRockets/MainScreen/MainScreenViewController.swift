//
//  MainScreenViewController.swift
//  SpaceXRockets
//
//  Created by Maksim on 13.04.2022.
//

import UIKit

class MainScreenViewController: UIViewController {
   
    private let mainView = UIView()
    private let verticalScrollView = UIScrollView()
    private let firstLabel = UILabel()
    private let secondLabel = UILabel()
    private let thirdLabel = UILabel()
    private let fourthLabel = UILabel()
    private let fiveLabel = UILabel()
    
    private let sixLabel = UILabel()
    private let sevenLabel = UILabel()
    private let eightLabel = UILabel()
    private let nineLabel = UILabel()
    private let tenLabel = UILabel()
    
    var viewModel: MainScreenViewModelProtocol!
    
    private let scrollView = UIScrollView()
    private let pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.numberOfPages = 4
        pageControl.backgroundColor = .lightGray
        pageControl.pageIndicatorTintColor = .white
        pageControl.currentPageIndicatorTintColor = .yellow
        return pageControl
    }()
    
    lazy var mainStackView: UIStackView = {
        let mainStackView = UIStackView()
        mainStackView.axis = .vertical
        mainStackView.spacing = -200
        mainStackView.alignment = .fill
        mainStackView.distribution = .fillEqually
        [self.verticalScrollView,
         self.scrollView].forEach { mainStackView.addArrangedSubview($0) }
        
        return mainStackView
    }()
    
    init(viewModel: MainScreenViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: MainScreenViewController.self), bundle: Bundle.main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        pageControl.addTarget(self,
                              action: #selector(pageControlDidChange(_:)),
                              for: .valueChanged)
        view.addSubview(verticalScrollView)
        view.addSubview(scrollView)
        view.addSubview(mainView)
        view.addSubview(mainStackView)
        view.addSubview(firstLabel)
        view.addSubview(secondLabel)
        view.addSubview(thirdLabel)
        view.addSubview(fourthLabel)
        view.addSubview(fiveLabel)
        view.addSubview(sixLabel)
        view.addSubview(sevenLabel)
        view.addSubview(eightLabel)
        view.addSubview(nineLabel)
        view.addSubview(tenLabel)
        view.addSubview(pageControl)
    }
    
    @objc private func pageControlDidChange (_ sender: UIPageControl) {
        let current = sender.currentPage
        scrollView.setContentOffset(CGPoint(x: CGFloat(current) * view.frame.size.width,
                                            y: 0), animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        mainView.frame = CGRect(x: 0, y: 0,
                                width: view.frame.size.width,
                                height: view.frame.size.height+300)
    
        mainView.layer.cornerRadius = 30
        mainView.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpeg")!)
        verticalScrollView.frame = CGRect(x: 0, y: 0,
                                          width: view.frame.size.width,
                                          height: view.frame.size.height+300)
        
        scrollView.frame = CGRect(x: 0, y: 0,
                                  width: view.frame.size.width,
                                  height: view.frame.size.height+200)
        scrollView.layer.cornerRadius = 30
        
        mainStackView.frame = CGRect(x: 0, y: 0,
                                    width: view.frame.size.width,
                                    height: view.frame.size.height-100)
      
        pageControl.frame = CGRect(x: 0, y: view.frame.size.height-100,
                                   width: view.frame.size.width,
                                   height: 70)
        pageControl.layer.cornerRadius = 20
        
        firstLabel.frame = CGRect(x: 20, y: 300,
                                  width: 150,
                                  height: 60)
        firstLabel.layer.masksToBounds = true
        firstLabel.layer.cornerRadius = 10
        firstLabel.backgroundColor = .yellow
        firstLabel.alpha = 0.8
        
        secondLabel.frame = CGRect(x: 20, y: 380,
                                  width: 150,
                                  height: 60)
        secondLabel.layer.masksToBounds = true
        secondLabel.layer.cornerRadius = 10
        secondLabel.backgroundColor = .yellow
        secondLabel.alpha = 0.8
        
        thirdLabel.frame = CGRect(x: 20, y: 460,
                                  width: 150,
                                  height: 60)
        thirdLabel.layer.masksToBounds = true
        thirdLabel.layer.cornerRadius = 10
        thirdLabel.backgroundColor = .yellow
        thirdLabel.alpha = 0.8
        
        fourthLabel.frame = CGRect(x: 20, y: 540,
                                  width: 150,
                                  height: 60)
        fourthLabel.layer.masksToBounds = true
        fourthLabel.layer.cornerRadius = 10
        fourthLabel.backgroundColor = .yellow
        fourthLabel.alpha = 0.8
        
        fiveLabel.frame = CGRect(x: 20, y: 620,
                                  width: 150,
                                  height: 60)
        fiveLabel.layer.masksToBounds = true
        fiveLabel.layer.cornerRadius = 10
        fiveLabel.backgroundColor = .yellow
        fiveLabel.alpha = 0.8
        
        sixLabel.frame = CGRect(x: 200, y: 300,
                                  width: 150,
                                  height: 60)
        sixLabel.layer.masksToBounds = true
        sixLabel.layer.cornerRadius = 10
        sixLabel.backgroundColor = .yellow
        sixLabel.alpha = 0.8
        sixLabel.textAlignment = .center

        sevenLabel.frame = CGRect(x: 200, y: 380,
                                  width: 150,
                                  height: 60)
        sevenLabel.layer.masksToBounds = true
        sevenLabel.layer.cornerRadius = 10
        sevenLabel.backgroundColor = .yellow
        sevenLabel.alpha = 0.8
        
        eightLabel.frame = CGRect(x: 200, y: 460,
                                  width: 150,
                                  height: 60)
        eightLabel.layer.masksToBounds = true
        eightLabel.layer.cornerRadius = 10
        eightLabel.backgroundColor = .yellow
        eightLabel.alpha = 0.8
        
        nineLabel.frame = CGRect(x: 200, y: 540,
                                  width: 150,
                                  height: 60)
        nineLabel.layer.masksToBounds = true
        nineLabel.layer.cornerRadius = 10
        nineLabel.backgroundColor = .yellow
        nineLabel.alpha = 0.8
        
        tenLabel.frame = CGRect(x: 200, y: 620,
                                  width: 150,
                                  height: 60)
        tenLabel.layer.masksToBounds = true
        tenLabel.layer.cornerRadius = 10
        tenLabel.backgroundColor = .yellow
        tenLabel.alpha = 0.8
            
        if scrollView.subviews.count == 2 {
            configureScrollView()
        }
    }
    
    private func configureScrollView() {
        scrollView.contentSize = CGSize(width: view.frame.size.width*4,
                                        height: scrollView.frame.size.height)
        scrollView.isPagingEnabled = true
     
        let images: [UIImage] = [
        UIImage(named: "rocket1.jpeg")!,
        UIImage(named: "rocket2.jpeg")!,
        UIImage(named: "rocket3.jpeg")!,
        UIImage(named: "rocket4.jpeg")!
        ]
        
        for x in 0..<images.count {
            
            let page = UIImageView(frame: CGRect(x: CGFloat(x) * view.frame.size.width,
                                                  y: 0, width: view.frame.size.width,
                                                  height: scrollView.frame.size.height))
            
            page.image = images[x]
            scrollView.addSubview(page)
        }
    }
}

extension MainScreenViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(floorf(Float(scrollView.contentOffset.x) / Float(scrollView.frame.size.width)))
    }
}
